extends Button

export (NodePath) var direction: NodePath

onready var parent: Container = get_parent()
onready var menu: Container = get_node(direction)

func _ready() -> void:
	connect('pressed', self, 'switch_menu')

func switch_menu() -> void:
	if !menu:
		printerr('invalid NodePath: "%s"' % direction)
		return
	parent.visible = false
	menu.visible = true
