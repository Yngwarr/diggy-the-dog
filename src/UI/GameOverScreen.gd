extends CanvasLayer

export (NodePath) var dawg_path

onready var PlayAgain: Button = $Menu/GameOverMenu/PlayAgain
onready var Anim: AnimationPlayer = $AnimationPlayer
onready var dawg = get_node(dawg_path)

func _ready() -> void:
	PlayAgain.connect('visibility_changed', self, 'focus_button')
	dawg.connect('win', self, 'appear')

# TODO connect appear to a signal
func appear() -> void:
	Global.game_over = true
	get_tree().paused = true
	Anim.play('show')

func focus_button():
	if !PlayAgain.visible: return
	PlayAgain.grab_focus()
