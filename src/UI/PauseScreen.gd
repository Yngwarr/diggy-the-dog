extends CanvasLayer

export (String) var quit_direction: String

var popup_active := false

onready var Anim: AnimationPlayer = $AnimationPlayer
onready var Menu: Control = $Menu
onready var Resume: Button = $Menu/PauseMenu/Resume
onready var Quit: Button = $Menu/PauseMenu/Quit
onready var OptionsMenu: Container = $Menu/OptionsMenu
onready var Popup: ConfirmationDialog = $Menu/ConfirmQuit

func _ready() -> void:
	Resume.connect('pressed', self, 'unpause')
	Quit.connect('pressed', self, 'confirm_quit')
	Popup.connect('confirmed', self, 'quit_game')
	Popup.connect('about_to_show', self, 'set_popup_active')
	Popup.connect('popup_hide', self, 'unset_popup_active')
	Resume.connect('visibility_changed', self, 'focus_button')

func _input(event):
	if Global.game_over: return
	if event.is_action_pressed('pause'):
		if get_tree().paused && !OptionsMenu.visible && !popup_active:
			unpause()
		else:
			pause()

func pause() -> void:
	get_tree().paused = true
	Anim.play('show')

func unpause() -> void:
	Anim.play_backwards('show')
	get_tree().paused = false

func confirm_quit() -> void:
	Popup.popup_centered()

func quit_game() -> void:
	get_tree().paused = false
	Global.switch_scene(quit_direction)

func set_popup_active():
	popup_active = true
	Menu.visible = false

func unset_popup_active():
	popup_active = false
	Menu.visible = true

func focus_button():
	if !Resume.visible: return
	Resume.grab_focus()
