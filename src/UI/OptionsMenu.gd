extends VBoxContainer

export (NodePath) var back_direction: NodePath
export (NodePath) var remap_direction: NodePath
export (NodePath) var focused_button_path: NodePath

onready var SFX: CheckButton = $SFX
onready var Music: CheckButton = $Music

onready var Back: Button = $Back
onready var Remap: Button = $Remap
onready var back_menu: Container = get_node(back_direction)
onready var remap_menu: Container = get_node(remap_direction)
onready var focused_button: Button = get_node(focused_button_path)

func _ready() -> void:
	Back.connect('pressed', self, 'switch_back')
	Remap.connect('pressed', self, 'switch_remap')
	focused_button.connect('visibility_changed', self, 'focus_button')
	sync_options()

func _input(event) -> void:
	if !visible: return
	if event.is_action_pressed('ui_cancel'):
		switch_back()
		get_tree().set_input_as_handled()

# synchronizes option keys with a global object
func sync_options() -> void:
	SFX.pressed = Global.options[SFX.option_key]
	Music.pressed = Global.options[Music.option_key]

# hides itself in favor of back_menu
func switch_back() -> void:
	if !back_menu:
		printerr('invalid NodePath: "%s"' % back_direction)
		return
	visible = false
	back_menu.visible = true

func switch_remap() -> void:
	if !remap_menu:
		printerr('invalid NodePath: "%s"' % remap_direction)
		return
	visible = false
	remap_menu.visible = true

# focuses focused_button to enable keyboard-only navigation
func focus_button():
	focused_button.grab_focus()
