extends Node

class_name LevelParser

export (NodePath) var tile_map_path
export (NodePath) var game_path
export (NodePath) var foe_container_path
export (NodePath) var torch_container_path
export (NodePath) var dawg_path
export (PackedScene) var mole
export (PackedScene) var potat
export (PackedScene) var torch
export (PackedScene) var elder
export (PackedScene) var poster
export (PackedScene) var unicorn
export (PackedScene) var ending_trigger

onready var game = get_node(game_path)
onready var tile_map = get_node(tile_map_path)
onready var foe_container = get_node(foe_container_path)
onready var torch_container = get_node(torch_container_path)
onready var dawg = get_node(dawg_path)

func _ready():
	var file = File.new()
	file.open('res://data/map.json', File.READ)
	var parse_res = JSON.parse(file.get_as_text())
	file.close()
	if parse_res.error:
		print("Error: can't read data/map.json, line %s: %s" % [parse_res.error_line, parse_res.error_string])
		return
	var map_info = parse_res.result
	populate_tilemap(map_info.walls)
	spawn_entities(map_info.entities)

func populate_tilemap(walls):
	for tile in walls:
		tile_map.set_cell(tile.p[0], tile.p[1], tile.t)

func spawn_entities(info):
	for x in info:
		match x.id:
			"Dawg":
				dawg.position.x = x.p[0]
				dawg.position.y = x.p[1]
			"Torch":
				var inst = torch.instance()
				torch_container.add_torch(inst, Vector2(x.p[0], x.p[1]))
			"Mole":
				spawn_foe(mole, x)
			"Potat":
				spawn_foe(potat, x)
			"Elder":
				spawn(elder, x)
			"Poster":
				spawn(poster, x)
			"Unicorn":
				spawn(unicorn, x)
			"EndingTrigger":
				var inst = ending_trigger.instance()
				inst.position.x = x.p[0]
				inst.position.y = x.p[1]
				inst.dawg = dawg
				game.add_child(inst)

func spawn(scene, info):
	var inst = scene.instance()
	inst.position.x = info.p[0]
	inst.position.y = info.p[1]
	game.add_child(inst)

func spawn_foe(scene, info):
	var inst = scene.instance()
	inst.position.x = info.p[0]
	inst.position.y = info.p[1]
	inst.bound = info.b
	foe_container.add_foe(inst)
