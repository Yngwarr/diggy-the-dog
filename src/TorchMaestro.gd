extends Node2D

signal spawn_changed

func add_torch(torch, pos):
	torch.position = pos
	torch.connect('lit', self, 'change_spawn')
	add_child(torch)

func change_spawn(torch):
	for t in get_children():
		print('whoop')
		if t == torch: continue
		t.anim.play('idle')
	emit_signal('spawn_changed', torch.position)
