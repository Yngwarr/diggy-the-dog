extends Area2D

signal lit

onready var anim = $AnimationPlayer

func _ready():
	connect('body_entered', self, 'body_entered')
	anim.connect('animation_finished', self, 'animation_finished')
	anim.play('idle')

func body_entered(body):
	if !body.is_in_group('player'): return
	emit_signal('lit', self)
	anim.play('light')

func animation_finished(animation):
	if animation != 'light': return
	anim.play('lit')
