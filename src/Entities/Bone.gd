extends KinematicBody2D

class_name Bone

signal picked
signal hit

const SPEED = 400.0

onready var anim = $AnimationPlayer
onready var area = $Area
onready var default_position = position
onready var gravity = ProjectSettings.get('physics/2d/default_gravity')
onready var hit_particles = $HitParticles

enum State { IDLE, CHARGED, UNCHARGED, FALLEN }

var velocity = Vector2.ZERO
var state = State.IDLE

# private
func active(on: bool):
	if !on:
		position = default_position
		anim.stop()
	else:
		anim.play('rotate')
	visible = on
	set_process(on)
	set_physics_process(on)

func throw(pos, dir):
	if state != State.IDLE: return false
	position = pos
	velocity = dir * SPEED
	active(true)
	state = State.CHARGED
	return true

# signal
func body_entered(body):
	if !body.is_in_group('player'): return
	pick()

func pick():
	emit_signal('picked')
	active(false)
	# current state doesn't matter
	state = State.IDLE

func _ready():
	add_to_group('weapon')
	active(false)
	area.connect('body_entered', self, 'body_entered')

func print_state():
	var s = ''
	match state:
		State.IDLE: s = 'IDLE'
		State.CHARGED: s = 'CHARGED'
		State.UNCHARGED: s = 'UNCHARGED'
		State.FALLEN: s = 'FALLEN'
	print(s)

func play_particles(direction):
	hit_particles.process_material.direction = Vector3(direction.x, direction.y, 0.0)
	hit_particles.emitting = true

func _physics_process(delta):
	var collision = null

	if state == State.FALLEN:
		move_and_slide(velocity, Vector2.UP)
	else:
		collision = move_and_collide(velocity * delta)

	match state:
		State.CHARGED:
			if !collision: return
			velocity.x = -velocity.x
			play_particles(velocity.normalized())
			emit_signal('hit', collision.collider)
			state = State.UNCHARGED
		State.UNCHARGED:
			if !collision: return
			velocity.x = -velocity.x
			play_particles(velocity.normalized())
			emit_signal('hit', collision.collider)
			state = State.FALLEN
		State.FALLEN:
			if is_on_floor():
				if anim.is_playing():
					anim.stop(false)
				velocity = Vector2.ZERO
				return
			velocity.y += gravity * delta
