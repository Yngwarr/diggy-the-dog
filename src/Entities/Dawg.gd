extends Actor

class_name Dawg

signal win

const FLOOR_DETECT_DISTANCE = 20.0

export (NodePath) var bone_path
export (NodePath) var torch_maestro_path
export (NodePath) var foe_maestro_path

onready var platform_detector = $PlatformDetector
onready var anim = $AnimationPlayer
onready var sprite = $Sprite
onready var jump_particles = $JumpParticles
onready var foe_maestro = get_node(foe_maestro_path)
onready var torch_maestro = get_node(torch_maestro_path)

onready var bone = get_node(bone_path)
onready var bone_view = $Sprite/BoneView
onready var bone_start: Position2D = $BoneStart
onready var bone_start_x = bone_start.position.x

onready var wall_detectors = [$WallDetectors/L, $WallDetectors/R]

var has_bone = true
var throwing_bone = false
var can_double_jump = false
var respawn_pos = Vector2.ZERO

func play_ending():
	set_process(false)
	set_physics_process(false)
	anim.play('ending')

func _ready():
	add_to_group('player')
	bone.connect('picked', self, 'pick_bone')
	torch_maestro.connect('spawn_changed', self, 'change_spawn')
	foe_maestro.connect('player_tagged', self, 'respawn')
	set_bone_view(true)

func change_spawn(pos):
	respawn_pos = pos

func respawn():
	position = respawn_pos
	bone.pick()
	set_bone_view(true)

func _process(_delta):
	# this is a horrible flow to throw the bone, tbh
	if Input.is_action_just_pressed('game_throw') and has_bone and not is_near_wall():
		throwing_bone = true

func _physics_process(_delta):
	var direction = get_direction()

	var is_jump_interrupted = Input.is_action_just_released("game_jump") and _velocity.y < 0.0
	_velocity = calculate_move_velocity(_velocity, direction, speed, is_jump_interrupted)

	var snap_vector = Vector2.ZERO
	if direction.y == 0.0:
		snap_vector = Vector2.DOWN * FLOOR_DETECT_DISTANCE
	var is_on_platform = platform_detector.is_colliding()
	_velocity = move_and_slide_with_snap(
		_velocity, snap_vector, FLOOR_NORMAL, not is_on_platform, 4, 0.9, false
	)

	if direction.x != 0:
		if direction.x > 0:
			sprite.scale.x = 1
			wall_detectors[0].enabled = false
			wall_detectors[1].enabled = true
			bone_start.position.x = bone_start_x
		else:
			sprite.scale.x = -1
			wall_detectors[0].enabled = true
			wall_detectors[1].enabled = false
			bone_start.position.x = -bone_start_x

	var animation = get_new_animation()
	if animation != anim.current_animation:
		anim.play(animation)

# animation
func throw_bone():
	throwing_bone = false
	if is_near_wall():
		set_bone_view(true)
		return
	var thrown = bone.throw(bone_start.global_position,
		Vector2.RIGHT if bone_start.position.x > 0 else Vector2.LEFT)
	if !thrown: return
	set_bone_view(false)

func set_bone_view(on: bool):
	has_bone = on
	bone_view.visible = on

# signal
func pick_bone():
	set_bone_view(true)
	can_double_jump = true
	# TODO play a particle effect

# private
func is_near_wall() -> bool:
	for detector in wall_detectors:
		if detector.is_colliding(): return true
	return false

func get_direction():
	var dir = Vector2(Input.get_action_strength("game_right") - Input.get_action_strength("game_left"), 0)
	if Input.is_action_just_pressed("game_jump") and (is_on_floor() || can_double_jump):
		if can_double_jump and not is_on_floor():
			jump_particles.emitting = true
		dir.y = -1
		can_double_jump = false
	return dir

# This function calculates a new velocity whenever you need it.
# It allows you to interrupt jumps.
func calculate_move_velocity(
		linear_velocity,
		direction,
		speed,
		is_jump_interrupted
	):
	var velocity = linear_velocity
	velocity.x = speed.x * direction.x
	if direction.y != 0.0:
		velocity.y = speed.y * direction.y
	if is_jump_interrupted:
		# Decrease the Y velocity by multiplying it, but don't set it to 0
		# as to not be too abrupt.
		velocity.y *= 0.6
	return velocity

func get_new_animation():
	if throwing_bone: return 'throw'
	if is_on_floor():
		if abs(_velocity.x) > 0.1:
			return 'run'
		return 'idle'
	if _velocity.y > 0:
		return 'falling'
	return 'jumping'

func game_over():
	emit_signal('win')
