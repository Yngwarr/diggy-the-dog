extends Actor

class_name Foe

signal player_tagged

export var bound = 200

onready var anim = $AnimationPlayer
onready var sprite = $Sprite
onready var stun_timer = $StunTimer
onready var hurt_particles = $HurtParticles
onready var rotate_particles = $RotateParticles

var direction = Vector2.LEFT
var path = [0, 0]
var foe_maestro

var stunned = false

# private
func stun():
	if stunned: return
	stunned = true
	stun_timer.start()
	anim.play('stun')
	hurt_particles.emitting = true

# signal
func unstun():
	stunned = false
	anim.play('patrol')

func _ready():
	path[0] = bound
	path[1] = position.x
	stun_timer.connect('timeout', self, 'unstun')
	speed.x = 100.0
	anim.play('patrol')

func play_rotate_particles(dir):
	rotate_particles.process_material.direction.x = dir.x
	rotate_particles.emitting = true

func _physics_process(_delta):
	if position.x < path[0]:
		direction = Vector2.RIGHT
		sprite.scale.x = -1
		play_rotate_particles(Vector2.LEFT)
	elif position.x > path[1]:
		direction = Vector2.LEFT
		sprite.scale.x = 1
		play_rotate_particles(Vector2.RIGHT)

	_velocity.x = 0 if stunned else speed.x * direction.x
	_velocity = move_and_slide(_velocity, Vector2.UP)

	if stunned: return
	for i in get_slide_count():
		var col = get_slide_collision(i)
		if !col.collider.is_in_group('player'): continue
		emit_signal('player_tagged')

