extends Node

signal player_tagged

export (NodePath) var bone_path

onready var bone: Bone = get_node(bone_path)

func _ready():
	bone.connect('hit', self, 'hit')

func add_foe(foe):
	foe.connect('player_tagged', self, 'player_tagged')
	add_child(foe)

func player_tagged():
	emit_signal('player_tagged')

func hit(body):
	for x in get_children():
		if x == body: x.stun()
